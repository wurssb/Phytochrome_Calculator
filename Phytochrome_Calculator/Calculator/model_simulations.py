import numpy as np
from scipy.integrate import odeint

def model_simulation(p,s,e,m,t):

    simulations = []

    for pi in range(0,len(p)):

        parameters = {'kdr':p[pi][0],'kdfr':p[pi][1],'k_r1':p[pi][2],'k_r2':p[pi][3],'k_31':p[pi][4],'k_32':p[pi][5],'k_41':p[pi][6],'k_42':p[pi][7],'k_5':p[pi][8],'k_in':p[pi][9],'M':p[pi][10],'a':p[pi][11],'b':p[pi][12]}
        
        nms_out = []
        irr_out = []
        for i in range(0,len(e)):
            nms = []
            irr = []
            if len(e[i][:,0]) > 1:
                for j in range(0,len(e[i][:,0])):
                    if (e[i][j,0] >= min(s[:,0])) and (e[i][j,0] <= max(s[:,0])):
                        nms.append(e[i][j,0])
                        irr.append(e[i][j,1])
                
                for j in range(0,len(s[0,:])/2):
                    sigma_r = np.interp(nms,s[:,0],s[:,1+2*j])
                    sigma_fr = np.interp(nms,s[:,0],s[:,2+2*j])
                    parameters['k1'] = np.dot(sigma_r,irr)*60*1E-6
                    parameters['k2'] = np.dot(sigma_fr,irr)*60*1E-6
                    if m == 'Analytic':
                        model = call_analytic(parameters)
                    elif m == 'Numeric':
                        model = call_numeric(parameters,t)
                    simulations.append(model)
                nms_out.append(nms)
                irr_out.append(irr)
            else:
                nms.append(float(e[i][:,0]))
                irr.append(float(e[i][:,1]))
                nms_out.append(float(e[i][:,0]))
                irr_out.append(float(e[i][:,1]))
                for j in range(0,len(s[0,:])/2):
                    sigma_r = np.interp(nms,s[:,0],s[:,1+2*j])
                    sigma_fr = np.interp(nms,s[:,0],s[:,2+2*j])
                    parameters['k1'] = np.dot(sigma_r,irr)*60*1E-6
                    parameters['k2'] = np.dot(sigma_fr,irr)*60*1E-6
                    if m == 'Analytic':
                        model = call_analytic(parameters)
                    elif m == 'Numeric':
                        model = call_numeric(parameters,t)
                    simulations.append(model)

    return simulations,nms_out,irr_out

def call_analytic(parameters):

    k1 = parameters['k1']
    k2 = parameters['k2']
    kr1 = parameters['k_r1']
    kr2 = parameters['k_r2']

    D0 = (k2+kr1)*(k2+kr2)/(k1**2 + (k2+kr2)*(2*k1+k2+kr1))
    D1 = 2*k1*(k2+kr2)/(k1**2 + (k2+kr2)*(2*k1+k2+kr1))
    D2 = k1**2/(k1**2 + (k2+kr2)*(2*k1+k2+kr1))
    Pfr = k1*(k1+k2+kr2)/(k1**2 + (k2+kr2)*(2*k1+k2+kr1))
    Pr = 1-Pfr

    values = [D0,D1,D2,Pr,Pfr]

    return values

def call_numeric(parameters,time):

    t = np.linspace(0,int(time),int(time)*1000+1)
    y0 = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    soln = odeint(Model,y0,t,(parameters,))

    M = parameters['M']

    D0 = (soln[-1,0]+soln[-1,3]+M*soln[-1,6])/(np.sum(soln[-1,0:6])+M*np.sum(soln[-1,6:9]))
    D1 = (soln[-1,1]+soln[-1,4]+M*soln[-1,7])/(np.sum(soln[-1,0:6])+M*np.sum(soln[-1,6:9]))
    D2 = (soln[-1,2]+soln[-1,5]+M*soln[-1,8])/(np.sum(soln[-1,0:6])+M*np.sum(soln[-1,6:9]))
    Pr = D0 + D1/2
    Pfr = D2 + D1/2
    Pfr_nuc = (soln[-1,4]/2 + soln[-1,5] + M*(soln[-1,7]/2 + soln[-1,8]))/(np.sum(soln[-1,0:6])+M*np.sum(soln[-1,6:9]))
    Pfr_Pfr_nuc = (soln[-1,5]+M*soln[-1,8])/(np.sum(soln[-1,0:6])+M*np.sum(soln[-1,6:9]))
    phy_sp = M*np.sum(soln[-1,6:9])/(np.sum(soln[-1,0:6])+M*np.sum(soln[-1,6:9]))

    values = [D0,D1,D2,Pr,Pfr,Pfr_nuc,Pfr_Pfr_nuc,phy_sp]

    return values

def Model(y,t,parameters):

    k1 = parameters['k1']
    k2 = parameters['k2']
    kdr = parameters['kdr']
    kdfr = parameters['kdfr']
    kr1 = parameters['k_r1']
    kr2 = parameters['k_r2']
    k31 = parameters['k_31']
    k32 = parameters['k_32']
    k41 = parameters['k_41']
    k42 = parameters['k_42']
    k5 = parameters['k_5']
    kin = parameters['k_in']
    M = parameters['M']
    a = parameters['a']
    b = parameters['b']

    dy = np.zeros((9))
    #D0c
    dy[0] = kdr*(1-y[0]) + (k2+kr1)*y[1] - 2*k1*y[0]
    #D1c
    dy[1] = 2*k1*y[0] + 2*(k2+kr2)*y[2] - (k1+kin+k2+kr1)*y[1] - kdr*y[1]*(1 + kdfr/(1+a*kdfr*(b+y[1]+y[2])))
    #D2c
    dy[2] = k1*y[1] - (2*(k2+kr2)+kin)*y[2] - kdr*y[2]*(1 + kdfr/(1+a*kdfr*(b+y[1]+y[2])))
    #D0n
    dy[3] = (k2+kr1)*y[4] + M*k5*y[6] - y[3]*(kdr + 2*k1)
    #D1n
    dy[4] = kin*y[1] + 2*y[5]*(kr2+k2) + M*k41*y[7] + 2*k1*y[3] - y[4]*(k1 + M*k31 + kr1 + k2) - kdr*y[4]*(1 + kdfr*b/((1 + a*kdfr*(y[1]+y[2]+b))*(1+a*kdfr*(y[4]+y[5]))))
    #D2n
    dy[5] = kin*y[2] + M*k42*y[8] + k1*y[4] - y[5]*(M*k32 + 2*(kr2 + k2)) - kdr*y[5]*(1 + kdfr*b/((1 + a*kdfr*(y[1]+y[2]+b))*(1+a*kdfr*(y[4]+y[5]))))
    #D0s
    dy[6] = k2*y[7] - y[6]*(k5 + kdr + 2*k1)
    #D1s
    dy[7] = k31*y[4] + 2*k2*y[8] + 2*k1*y[6] - y[7]*(kdr + k1 + k41 + k2)
    #D2s
    dy[8] = k32*y[5] + k1*y[7] - y[8]*(kdr + k42 + 2*k2)

    return dy