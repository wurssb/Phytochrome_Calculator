from optparse import OptionParser
from unpack_file import *
from model_simulations import *
from write_to_screen import *
from write_to_file import *

def calculator(options=None):

    ks = unpack_file(options.k,0)
    sigs = unpack_file(options.s,1)
    exps = unpack_file(options.e,2)

    simulations,nms,irr = model_simulation(ks,sigs,exps,options.m,options.t)

    write_to_screen(options,ks,sigs,exps,nms,irr,simulations)

    write_to_file(options,ks,sigs,exps,nms,irr,simulations)

    return simulations

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option('-k','--systems_parameters',dest='k',help='systems parameters')
    parser.add_option('-s','--photoconversion_spectra',dest='s',help='photoconversion spectra')
    parser.add_option('-e','--experimental_conditions',dest='e',help='experimental conditions')
    parser.add_option('-m','--model_solving',dest='m',help='model simulation')
    parser.add_option('-t','--simulation_time',dest='t',help='simulation time')
    parser.add_option('-o','--output_file',dest='o',help='output file')
    (options,args) = parser.parse_args()
    output = calculator(options)
