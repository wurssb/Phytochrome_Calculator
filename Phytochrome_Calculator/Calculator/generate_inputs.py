import numpy as np
import os

def input_parameters():

    default = {'kdr': 0.00061, 'kdfr': 49.57, 'k_r1': 0.497, 'k_r2': 0.0051, 'k_31': 1.36,
                  'k_32': 4.43, 'k_41': 4.1, 'k_42': 0.13, 'k_5': 0.03, 'k_in': 58.4,
                  'M': 6.0, 'a': 0.56, 'b': 9.0}

    option = raw_input(" Would you like to change any system parameters? [y/n] \n")
    if option == 'y' or option == 'Y':
        file_option = raw_input(" Do you have an input file? [y/n] \n")
        if file_option == 'y' or file_option == 'Y':
            file = raw_input(" Enter filename: \n e.g. system_parameters.txt \n By default all files are stored in the folder /Phytochrome_Calculator/Input_files \n")
            curdir = os.getcwd()
            curpath = os.path.join(curdir,"Input_files")
            filename = os.path.join(curpath,file)
        elif file_option == 'n' or file_option == 'N':
            parameters_change = raw_input(" Which parameters would you like to change? \n e.g. \"k_r1\" or \"k_r1,k_r2\" \n Note: the list should not end with a comma \n")
            parameters_list = parameters_change.split(',')
            parameters_values = raw_input(" What value are these parameters? \n e.g. \"0.01\" or \"0.01,0.1\" \n Note: the list should not end with a comma \n")
            values_list = parameters_values.split(',')

            new_option = default
            for i in range(0,len(parameters_list)):
                new_option[parameters_list[i]] = float(values_list[i])

            new_parameters = []
            for i in ['kdr','kdfr','k_r1','k_r2','k_31','k_32','k_41','k_42','k_5','k_in','M','a','b']:
                new_parameters.append(new_option[i])
            curdir = os.getcwd()
            curpath = os.path.join(curdir,"Input_files")
            filename = os.path.join(curpath,"system_parameters_new.txt")
            file = open(filename, "w")
            file.write('# File contains default values for system parameters.\n')
            file.write(
                '# These can be changed or further options for values can be included for a parameter in each column. The steady state amounts of each system component shall be calculated for each parameter set.\n')
            file.write('# [k_dr,k_dfr,k_r1,k_r2,k_31,k_32,k_41,k_42,k_5,k_in,M,a,b]\n')
            np.savetxt(file, new_parameters)
            file.close()
    elif option == 'n' or option == 'N':
        new_parameters = []
        for i in ['kdr', 'kdfr', 'k_r1', 'k_r2', 'k_31', 'k_32', 'k_41', 'k_42', 'k_5', 'k_in', 'M', 'a', 'b']:
            new_parameters.append(default[i])
        curdir = os.getcwd()
        curpath = os.path.join(curdir,"Input_files")
        filename = os.path.join(curpath,"system_parameters.txt")
        file = open(filename, "w")
        file.write('# File contains default values for system parameters.\n')
        file.write('# These can be changed or further options for values can be included for a parameter in each column. The steady state amounts of each system component shall be calculated for each parameter set.\n')
        file.write('# [k_dr,k_dfr,k_r1,k_r2,k_31,k_32,k_41,k_42,k_5,k_in,M,a,b]\n')
        np.savetxt(file, new_parameters)
        file.close()
    return filename

def input_spectra():

    option = raw_input(" Would you like to use the default photoconversion spectra? [y/n] \n")
    if option == 'y' or option == 'Y':
        curdir = os.getcwd()
        curpath = os.path.join(curdir,"Input_files")
        filename = os.path.join(curpath,"photoconversion_default.txt")
    elif option == 'n' or option == 'N':
        file = raw_input(" Put name of new photoconversion spectra file: \n e.g. photoconversion_spectra.txt \n By default the code will assumes the file is in the folder /Phytochrome_Calculator/Input_files \n")
        curdir = os.getcwd()
        curpath = os.path.join(curdir,"Input_files")
        filename = os.path.join(curpath,file)

    return filename

def input_experiments():

    option = raw_input(" Do you have an input file of measured light intensities? [y/n] \n")
    if option == 'y' or option == 'Y':
        file = raw_input(" Put filename of new light condition: \n e.g. experimental_conditions.txt \n By default, the code will assumes the file is in the folder /Phytochrome_Calculator/Input_files \n")
        curdir = os.getcwd()
        curpath = os.path.join(curdir,"Input_files")
        filename = os.path.join(curpath,file)

    elif option == 'n' or option == 'N':
        wavelengths = raw_input(" Please list which wavelengths (in nm) you are interested in: \n e.g. \"660,740\" \n Note: the list can not end with a comma \n")
        nms_list = wavelengths.split(',')
        intensities = raw_input(" Please indicate their respective intensities (umol.m^-2.s^-1): \n e.g. \"5,0.5\" \n Note: the list can not end with a comma \n")
        I_list = intensities.split(',')
        integration = raw_input(" Do you want to know species amounts at each wavelength or integrated over all entered wavelengths? \n Enter '1' for each wavelength or '2' for data integrated over all wavelengths \n")

        curdir = os.getcwd()
        curpath = os.path.join(curdir,"Input_files")
        filename = os.path.join(curpath,"experimental_conditions_new.txt")
        file = open(filename, "w")
        file.write('# This file contains the experimental conditions for which one wishes to calculated the percentages of phytochrome subspecies.\n')
        file.write('# Multiple conditions can be compared, both from light distributions and single wavelengths.\n')
        file.write('# The column headers must read [nm1 N1 nm2 N2 etc] such that a column of measured wavelengths are followed by a column of intensity measurements. One can also just use single values.\n')
        file.write('# [nm,N,nm,N,nm,N]\n')

        if integration == '1':
            for i in range(0,len(nms_list)):
                file.write(str(float(nms_list[i]))+","+str(float(I_list[i]))+",")
        elif integration == '2':
            nms_list_interp = np.linspace(float(nms_list[0]),float(nms_list[-1]),float(nms_list[-1])-float(nms_list[0])+1)
            I_list_interp = np.interp(nms_list_interp,[float(i) for i in nms_list],[float(i) for i in I_list])
            for i in range(0,len(nms_list_interp)):
                file.write(str(float(nms_list_interp[i]))+","+str(float(I_list_interp[i]))+"\n")

        file.close()

    return filename

def input_model():

    option = raw_input(" Would you like to solve the system analytically or numerically? [a/n] \n")
    if option == 'n' or option == 'N':
        model = 'Numeric'
        time = raw_input(" How long would you like to simulate the system for (in minutes)? \n")
    elif option == 'a' or option == 'A':
        model = 'Analytic'
        time = 'Infinity'

    return model,time

def input_output():

    file = raw_input(" Please enter filename for results file: \n e.g. output.txt \n By default the file will be saved in the folder /Phytochrome_Calculator/Output_files \n")
    curdir = os.getcwd()
    curpath = os.path.join(curdir,"Output_files")
    filename = os.path.join(curpath,file)

    return filename

def generate_inputs():

    inputs = []

    parameters = input_parameters()
    inputs.append(parameters)

    spectra = input_spectra()
    inputs.append(spectra)

    experiments = input_experiments()
    inputs.append(experiments)

    model,time = input_model()
    inputs.append(model)
    inputs.append(time)

    output = input_output()
    inputs.append(output)

    return inputs

if __name__=="__main__":
    inputs = generate_inputs()
